package main

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
)

func touchFile(uri fyne.URI) bool {
	exists, err := storage.Exists(uri)
	if err != nil {
		fmt.Println("Error with Exists: ", err.Error())
		return false
	}
	if exists {
		fmt.Println("File already exists")
		return true
	}

	canWrite, err := storage.CanWrite(uri)
	if err != nil {
		fmt.Println("Error with CanWrite: ", err.Error())
		return false
	}
	if !canWrite {
		fmt.Println("Cannot write to file for touch")
		return false
	}

	writer, err := storage.Writer(uri)
	if err != nil {
		fmt.Println("Error with Writer(): ", err.Error())
		return false
	}
	defer writer.Close()
	return true
}

func loadBytes(uri fyne.URI) []byte {
	canRead, err := storage.CanRead(uri)
	if err != nil {
		fmt.Println("Error with CanRead(): ", err.Error())
		return nil
	}
	if !canRead {
		fmt.Println("Cannot read file " + uri.String())
	}

	var success bool
	exists, err := storage.Exists(uri)
	if err != nil {
		fmt.Println("Error with Exists(): ", err.Error())
		return nil
	}
	if !exists {
		success = touchFile(uri)
		if !success {
			fmt.Println("Cannot load non-existant file")
			return nil
		}
		return nil
	}

	reader, err := storage.Reader(uri)
	if err != nil {
		fmt.Println("Error with Reader(): ", err.Error())
	}
	defer reader.Close()

	buffer := make([]byte, 1024)
	var byteData []byte
	for {
		n, err := reader.Read(buffer)
		if err != nil && err != io.EOF {
			fmt.Println("Error with Read(): ", err.Error())
			return nil
		}
		byteData = append(byteData, buffer[:n]...)
		if err == io.EOF {
			break
		}
	}

	return byteData
}

func saveBytes(uri fyne.URI, bytes []byte) bool {
	canWrite, err := storage.CanWrite(uri)
	if err != nil {
		fmt.Println("Error with CanWrite(): ", err.Error())
		return false
	}
	if !canWrite {
		fmt.Println("Cannot save data to file")
		return false
	}

	exists, err := storage.Exists(uri)
	if err != nil {
		fmt.Println("Error with Exists(): ", err.Error())
		return false
	}
	if !exists {
		touchFile(uri)
	}

	writer, err := storage.Writer(uri)
	if err != nil {
		fmt.Println("Error with Writer(): ", err.Error())
		return false
	}
	writer.Write(bytes)
	return true
}

func newURIFromRoot(app fyne.App, path string) fyne.URI {
	rootURI := app.Storage().RootURI()
	rootPath := strings.TrimPrefix(rootURI.String(), "file://")
	return storage.NewFileURI(rootPath + "/" + path)
}

func jsonToWorkouts(bytes []byte) []workout {
	type unparsedWorkout struct {
		Date   string
		Name   string
		Sets   int
		Reps   []int
		Weight float32
	}

	if len(bytes) == 0 {
		return make([]workout, 0)
	}

	var jsonData map[string][]unparsedWorkout
	err := json.Unmarshal(bytes, &jsonData)
	if err != nil {
		fmt.Println("Error with Unmarshal(): " + err.Error())
	}
	unparsedWorkouts := jsonData["workouts"]

	workouts := make([]workout, 0)
	for _, u := range unparsedWorkouts {
		parsedDate, err := time.Parse("2006-01-02", u.Date)
		if err != nil {
			fmt.Println("Error with Parse(): " + err.Error())
		}

		workout := workout{
			Date:   parsedDate,
			Name:   u.Name,
			Sets:   u.Sets,
			Reps:   u.Reps,
			Weight: u.Weight,
		}
		workouts = append(workouts, workout)
	}

	return workouts
}

func workoutsToJSON(workouts []workout) []byte {
	type unparsedWorkout struct {
		Date   string
		Name   string
		Sets   int
		Reps   []int
		Weight float32
	}

	unparsedWorkouts := make([]unparsedWorkout, 0)
	for _, w := range workouts {
		u := unparsedWorkout{
			Date:   w.Date.Format("2006-01-02"),
			Name:   w.Name,
			Sets:   w.Sets,
			Reps:   w.Reps,
			Weight: w.Weight,
		}
		unparsedWorkouts = append(unparsedWorkouts, u)
	}

	workoutJson := map[string][]unparsedWorkout{"workouts": unparsedWorkouts}
	bytes, err := json.Marshal(workoutJson)
	if err != nil {
		fmt.Println("Error with Marshal(): " + err.Error())
	}

	return bytes
}

func (w workout) repsAsString() string {
	repStrings := make([]string, 0)
	for _, rep := range w.Reps {
		repStrings = append(repStrings, strconv.Itoa(rep))
	}
	return strings.Join(repStrings, "-")
}

func (w workout) weightAsString() string {
	return strconv.FormatFloat(float64(w.Weight), 'f', -1, 32)
}
